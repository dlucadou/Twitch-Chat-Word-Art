# IRC Word Art

This is a Java application that takes IRC messages (originally intended for use
on Twitch but can be used with any IRC network) and creates Word Art out of them
by using Java's Robot class to interact with the website [Make WordArt](https://www.makewordart.com/).

This application pulls logs from mIRC log files. Because it has to use the Robot
to interact with the desktop, it is recommended you run this inside a VM to
ensure you do not lose control of your desktop.

Recommended setup: Google Chrome running maximized on a 1024x768 resolution
monitor.

Make sure to update the sentinel value in `src/wordart/Main.java` before starting
it so you will be able to stop it. Sometimes it borks itself due to the inherent
difficulty in automated GUI interaction and you have to stop it to clean up Word
Art it was unable to delete.